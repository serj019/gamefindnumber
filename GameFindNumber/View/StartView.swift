//
//  StartView.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//

import UIKit



class StartView: UIView {
    
    let titleLabel = UILabel(text: "My Awesome Game")
    let startButton = UIButton(blueWith: "Start New Game")

    init() {
        super.init(frame: CGRect())
        backgroundColor = .white
        setConstraints()
        setViews()
    }
    
    func setViews() {
        
    }
    
    func setConstraints() {
        addSubview(titleLabel)
        addSubview(startButton)
        
        titleLabel.translatesAutoresizingMaskIntoConstraints = false
        startButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            titleLabel.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 70),
            titleLabel.centerXAnchor.constraint(equalTo: centerXAnchor)])
        
        NSLayoutConstraint.activate([
            startButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -50),
            startButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            startButton.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
