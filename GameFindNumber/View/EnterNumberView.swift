//
//  EnterNumberView.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//

import UIKit

class EnterNumberView: UIView {
    
    var textField = UITextField("Guess the number")
    
    let startButton = UIButton(blueWith: "Enter the number")

    init() {
        super.init(frame: CGRect())
        backgroundColor = .white
        setConstraints()
        setViews()
    }
    
    func setViews() {
        textField.textAlignment = .center
        textField.keyboardType = .numberPad
        startButton.isEnabled = false
    }
    
    func setConstraints() {
        addSubview(textField)
        addSubview(startButton)
        
        textField.translatesAutoresizingMaskIntoConstraints = false
        startButton.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            textField.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 70),
            textField.centerXAnchor.constraint(equalTo: centerXAnchor),
            textField.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
        ])
        
        NSLayoutConstraint.activate([
            startButton.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -50),
            startButton.leftAnchor.constraint(equalTo: leftAnchor, constant: 16),
            startButton.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
