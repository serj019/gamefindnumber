//
//  CompGuessView.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//



import UIKit

class CompGuessView: UIView {

    let tryLabel = UILabel(text: "Try № 1")
    let titleLabel = UILabel(text: "Computer is guessing")
    let resultLabel = UILabel(text: "Your number is - 50?")
    let answerLabel = UILabel(text: "My number is...")
    
    let littleButton = UIButton(whiteWith: "<")
    let equalButton = UIButton(whiteWith: "=")
    let bigButton = UIButton(whiteWith: ">")
    
    init() {
        super.init(frame: CGRect())
        setViews()
        setConstraints()
    }
    
    func setViews() {
        backgroundColor = .white
        
        let labelsCenter = [tryLabel, titleLabel, resultLabel]
        for label in labelsCenter {
            label.textAlignment = .center
        }
        
    }
    
    func setConstraints() {
        let titleStack = UIStackView(views: [tryLabel, titleLabel], axis: .vertical, spacing: 16)
        let topStack = UIStackView(views: [titleStack, resultLabel], axis: .vertical, spacing: 40)
        
        let buttonsStack = UIStackView(views: [littleButton, equalButton, bigButton], axis: .horizontal, spacing: 6)
        let bottomStack = UIStackView(views: [answerLabel, buttonsStack], axis: .vertical, spacing: 8)
        addSubview(topStack)
        addSubview(bottomStack)
        topStack.translatesAutoresizingMaskIntoConstraints = false
        bottomStack.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            topStack.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 20),
            topStack.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            bottomStack.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor, constant: -50),
            bottomStack.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
