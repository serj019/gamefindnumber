//
//  RezultView.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 09.09.2022.
//

import UIKit

class RezultView: UIView {
    
    var labelScore = UILabel(text: "Scores:")
    var labelYourTries = UILabel(text: "Your's tries count:")
    var labelCompTries = UILabel(text: "Computers's tries count:")
    var labelWinner = UILabel(text: "Win")
    var buttonMenu = UIButton(blueWith: "Main Menu")

    init() {
        super.init(frame: CGRect())
       
        setConstrains()
        setView()
    }
    
    func setView() {
       backgroundColor = .white
        labelWinner.textAlignment = .center
        
    }
    
    func setConstrains() {
        
        let leftCenterStack = UIStackView(views: [labelYourTries,labelCompTries], axis: .vertical, spacing: 20)
        let bottomStack = UIStackView(views: [labelWinner,buttonMenu], axis: .vertical, spacing: 40)
        
        self.addSubview(leftCenterStack)
        self.addSubview(bottomStack)
        self.addSubview(labelScore)
        
        leftCenterStack.translatesAutoresizingMaskIntoConstraints=false
        bottomStack.translatesAutoresizingMaskIntoConstraints=false
        labelScore.translatesAutoresizingMaskIntoConstraints=false

        
        
        NSLayoutConstraint.activate([
            leftCenterStack.leftAnchor.constraint(equalTo: safeAreaLayoutGuide.leftAnchor, constant: 15),
            leftCenterStack.centerYAnchor.constraint(equalTo:  safeAreaLayoutGuide.centerYAnchor)
        
        ])
        
        NSLayoutConstraint.activate([
            bottomStack.bottomAnchor.constraint(equalTo:  safeAreaLayoutGuide.bottomAnchor, constant: -30),
            bottomStack.centerXAnchor.constraint(equalTo:  safeAreaLayoutGuide.centerXAnchor),
            bottomStack.leftAnchor.constraint(equalTo:  safeAreaLayoutGuide.leftAnchor, constant: 20)
        ])
        
        NSLayoutConstraint.activate([
            labelScore.topAnchor.constraint(equalTo:  safeAreaLayoutGuide.topAnchor, constant: 40),
            labelScore.centerXAnchor.constraint(equalTo:  safeAreaLayoutGuide.centerXAnchor)
        
        ])
        
    }
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

    

}
