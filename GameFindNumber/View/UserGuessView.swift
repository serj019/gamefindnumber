//
//  UserGuessView.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//

import UIKit

class UserGuessView: UIView {

    var tryLabel = UILabel(text: "Try № 1")
    var titleLabel = UILabel(text: "You are guessing")
    var textField = UITextField("Guess the number")
    var buttonGuess = UIButton(blueWith: "Guess")
    var labelAnswer = UILabel(text: "")
   
   init() {
        super.init(frame: CGRect())
       setViews()
       setConstraints()
    }
    
    func setViews() {
        backgroundColor = .white
        
        tryLabel.textAlignment = .center
        titleLabel.textAlignment = .center
        labelAnswer.textAlignment = .center
        textField.textAlignment = .center
        textField.keyboardType = .numberPad
        buttonGuess.isEnabled = false
    }
    
    func setConstraints() {
     
        let titleStack = UIStackView(views: [tryLabel,titleLabel], axis: .vertical, spacing: 20)
        
        let centerStack = UIStackView(views: [textField,labelAnswer], axis: .vertical, spacing: 40)
        
        self.addSubview(titleStack)
        self.addSubview(centerStack)
        self.addSubview(buttonGuess)
        
        titleStack.translatesAutoresizingMaskIntoConstraints=false
        centerStack.translatesAutoresizingMaskIntoConstraints=false
        buttonGuess.translatesAutoresizingMaskIntoConstraints=false
        
        NSLayoutConstraint.activate([
            titleStack.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor, constant: 50),
            titleStack.centerXAnchor.constraint(equalTo: centerXAnchor)
        ])
        
        NSLayoutConstraint.activate([
            centerStack.centerYAnchor.constraint(equalTo: centerYAnchor),
            centerStack.centerXAnchor.constraint(equalTo: centerXAnchor),
            centerStack.leftAnchor.constraint(equalTo: leftAnchor, constant: 16)
        ])
        
        NSLayoutConstraint.activate([
            buttonGuess.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -50),
            buttonGuess.centerXAnchor.constraint(equalTo: centerXAnchor),
            buttonGuess.leftAnchor.constraint(equalTo: leftAnchor, constant: 16)
        
        ])
        
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}
