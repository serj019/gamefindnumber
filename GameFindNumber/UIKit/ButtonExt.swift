//
//  ButtonExt.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//

import UIKit

extension UIButton {
    
    convenience init(blueWith title: String) {
        self.init(type: .system)
        self.backgroundColor = UIColor(named: "AccentColor")
        self.tintColor = .white
        self.setTitle(title, for: .normal)
        self.titleLabel?.font = .systemFont(ofSize: 16)
        self.layer.cornerRadius = 12
        self.translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(equalToConstant: 38).isActive = true
    }
    
    convenience init(whiteWith text: String) {
        self.init(type: .system)
        self.backgroundColor = .white
        self.setTitle(text, for: .normal)
        self.layer.cornerRadius = 8
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1
        self.titleLabel?.font = .systemFont(ofSize: 16)
        self.translatesAutoresizingMaskIntoConstraints = false
        self.heightAnchor.constraint(equalToConstant: 60).isActive = true
        self.widthAnchor.constraint(equalToConstant: 60).isActive = true
        self.tintColor = .black
    }
    
}
