//
//  LabelExt.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//

import UIKit

extension UILabel {
    
    convenience init(text: String) {
        self.init(frame: CGRect())
        self.text = text
        self.font = .systemFont(ofSize: 16)
    }
    
}
