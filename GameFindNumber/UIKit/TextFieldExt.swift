//
//  TextFieldExt.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//

import UIKit

extension UITextField {
    
    convenience init(_ placeholder: String) {
        self.init(frame: CGRect())
        
        backgroundColor = .white
        self.placeholder = placeholder
        layer.cornerRadius = 12
        font =  .systemFont(ofSize: 16)
        leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 0))
        leftViewMode = .always
        translatesAutoresizingMaskIntoConstraints = false
        heightAnchor.constraint(equalToConstant: 38).isActive = true
        layer.borderColor = UIColor.lightGray.cgColor
        layer.borderWidth = 1
    }
    
}
