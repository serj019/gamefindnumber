//
//  StackViewExt.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//

import UIKit

extension UIStackView {
    
    convenience init(views: [UIView],axis: NSLayoutConstraint.Axis,spacing: CGFloat)
    {
        
        self.init(arrangedSubviews: views)
        self.spacing = spacing
        self.axis = axis
    }
    
}
