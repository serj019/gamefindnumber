//
//  GameModel.swift
//  GameFindNumber
//
//  Created by Влад Мади on 17.01.2023.
//

import Foundation

class GameModel {
    var compTries = 0
    var playerTries = 0
    var min = 0
    var max = 100
    var currentResult = 0
    var truePlayerNumber: Int
    var trueCompNumber = 0
    
    init(truePlayerNumber: Int) {
        self.truePlayerNumber = truePlayerNumber
    }
    
    func getWinnerName() -> String {
        switch compTries {
        case let comp where comp < playerTries : return "Компьютер выиграл"
        case let comp where comp > playerTries : return "Вы выиграли"
        default: return "Ничья"
        }
    }
    
    func checkTruth(number: Int, trueNumber: Int, answer: Answer) -> Bool {
        switch number {
        case let num where trueNumber > num :
            return answer == .great ? true : false
        case let num where trueNumber < num :
            return answer == .less ? true : false
        default:
            return answer == .equal ? true : false
        }
    }
    
}

struct Player {
    var score = 0
}
