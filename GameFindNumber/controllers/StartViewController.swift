//
//  ViewController.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//

import UIKit

class StartViewController: UIViewController {
    
    let mainView = StartView()
    let testView = RezultView()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mainView
        addActions()
    }
    
    func addActions() {
        let showFindNumberAction = UIAction { _ in
            let vc = EnterNumberController()
            vc.modalPresentationStyle = .fullScreen
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
        mainView.startButton.addAction(showFindNumberAction, for: .touchUpInside)
    }

}
