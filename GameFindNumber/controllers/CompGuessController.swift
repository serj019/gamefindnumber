//
//  CompGuessController.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//

import UIKit

enum Answer {
    case less
    case equal
    case great
}

class CompGuessController: UIViewController {
    
    let mainView = CompGuessView()
    var result = 50
    let gameModel: GameModel
    
    init(gameModel: GameModel) {
        self.gameModel = gameModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view = mainView
        self.guess(answer: nil)
        addActions()
        
    }
    
    func addActions() {
        let littleAction = UIAction { _ in
            self.guess(answer: .less)
        }
        
        let equalAction = UIAction { _ in
            self.guess(answer: .equal)
        }
        
        let greatAction = UIAction { _ in
            self.guess(answer: .great)
        }
        
        mainView.bigButton.addAction(greatAction, for: .touchUpInside)
        mainView.equalButton.addAction(equalAction, for: .touchUpInside)
        mainView.littleButton.addAction(littleAction, for: .touchUpInside)
    }
    
    func guess(answer: Answer?) {

        mainView.tryLabel.text = "Try № \(gameModel.compTries)"
        if let answer = answer {
            guard gameModel.checkTruth(number: result, trueNumber: gameModel.truePlayerNumber, answer: answer) else {
                //Алёрт "Не врать!"
                print("Не сочиняй!!!")
                return
            }
            switch answer {
            case .less:
                self.gameModel.max = result - 1
                self.result = (self.gameModel.max + self.gameModel.min ) / 2
                self.gameModel.compTries += 1
            case .equal:
                let alert = UIAlertController(title: "Число \(self.result) отгадано!", message: "Количество попыток: \(gameModel.compTries)", preferredStyle: .alert)
                let okAction = UIAlertAction(title: "Теперь отгадываю я!", style: .default) { _ in
                    let vc = UserGuessController(gameModel: self.gameModel)
                    
//                    vc.modalPresentationStyle = .fullScreen
                    self.navigationController?.pushViewController(vc, animated: true)
                    
                }
                alert.addAction(okAction)
                self.present(alert, animated: true)
                self.gameModel.compTries += 1
            case .great:
                self.gameModel.min = result + 1
                self.result = (self.gameModel.max + self.gameModel.min ) / 2
                self.gameModel.compTries += 1
            }
            self.mainView.resultLabel.text = "Your number is \(self.result)?"
        } else {
            let result = 50
            self.result = result
            self.mainView.resultLabel.text = "Your number is \(result)?"
        }
        gameModel.currentResult = gameModel.compTries
    }
}
