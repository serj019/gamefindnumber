//
//  UserGuessController.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//

import UIKit

class UserGuessController: UIViewController {

    var mainView = UserGuessView()
    
    var result = 1
    var randomNummer = Int.random(in: 1...100)
    let gameModel: GameModel
    
    init(gameModel: GameModel) {
        self.gameModel = gameModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mainView
        mainView.textField.delegate = self
        addActions()
        
    }
    
    
    func addActions() {
        let action = UIAction { _ in
            self.checkAnswer(answer: self.result)
        }
        mainView.buttonGuess.addAction(action, for: .touchUpInside)
    }
    
    
    func checkAnswer(answer:Int) {
        if answer > randomNummer {
            mainView.labelAnswer.text = "Мое число меньше\(randomNummer)"
        }
        if answer == randomNummer  {
 
            let alert = UIAlertController(title: "Число \(self.randomNummer) отгадано!",
                                          message: "Количество попыток: \(self.gameModel.playerTries + 1)",
                                          preferredStyle: .alert)
          
            let alertAction = UIAlertAction(title: "Посмотреть результат", style: .default) { _ in
                let vc = RezultViewController(gameModel: self.gameModel)
                vc.gameModel.compTries = self.gameModel.currentResult - 1
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)

            }
            alert.addAction(alertAction)
            self.present(alert, animated: true)
            
        }
        if answer<randomNummer {
           
            mainView.labelAnswer.text = "Мое число больше\(randomNummer)"
        }
        self.gameModel.playerTries += 1
        mainView.tryLabel.text="Try №\(self.gameModel.playerTries)"
    }
}



extension UserGuessController:UITextFieldDelegate
{
    func textFieldDidChangeSelection(_ textField: UITextField) {
       
        guard let text = mainView.textField.text else { return }

        let number = Int(text)
        if let number = number,
            number <= 100 && number >= 1{
            result=number
            mainView.buttonGuess.isEnabled = true
        } else {
            mainView.buttonGuess.isEnabled = false
        }
    }
}
