//
//  EnterNumberController.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 01.09.2022.
//

import UIKit

class EnterNumberController: UIViewController {
    
    let mainView = EnterNumberView()
    var gameModel: GameModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view = mainView
        mainView.textField.delegate = self
        addActions()
        navigationController?.navigationBar.isHidden = true
    }
    
    func addActions() {
        let startAction = UIAction { _ in
            
            if let gameModel = self.gameModel {
                let vc = CompGuessController(gameModel: gameModel)
                vc.modalPresentationStyle = .fullScreen
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
        
        mainView.startButton.addAction(startAction, for: .touchUpInside)
    }
    
}

extension EnterNumberController: UITextFieldDelegate {
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        
        guard let text = mainView.textField.text else { return }
        
        let number = Int(text)
        if let number = number,
            number <= 100 && number >= 1 {
            mainView.startButton.isEnabled = true
            self.gameModel = GameModel(truePlayerNumber: number)
        } else {
            mainView.startButton.isEnabled = false
        }
    }
    
}
