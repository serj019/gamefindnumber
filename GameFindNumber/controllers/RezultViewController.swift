//
//  RezultViewController.swift
//  GameFindNumber
//
//  Created by Serj Potapov on 09.09.2022.
//

import UIKit

class RezultViewController: UIViewController {
    
    var mainView = RezultView()
    let gameModel: GameModel
    init(gameModel: GameModel) {
        self.gameModel = gameModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view = mainView
        addActions()
        takeData()
    }
    
    func addActions() {
        let showFindNumberAction = UIAction { _ in
            self.navigationController?.popToRootViewController(animated: true)
        }
        mainView.buttonMenu.addAction(showFindNumberAction, for: .touchUpInside)
    }
    
    func takeData() {
        mainView.labelCompTries.text = "Computers's tries count:\(gameModel.compTries)"
        mainView.labelYourTries.text = "Your's tries count:\(gameModel.playerTries)"
        
        let winText = gameModel.getWinnerName()
        mainView.labelWinner.text = winText
    }
}
